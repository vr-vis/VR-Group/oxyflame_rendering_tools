#include <cstdint>
#include <iostream>
#include <string>

#include <vtkCellDataToPointData.h>
#include <vtkDataArraySelection.h>
#include <vtkDataObject.h>
#include <vtkImageData.h>
#include <vtkResampleToImage.h>
#include <vtkSmartPointer.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkUnstructuredGrid.h>

// Usage: ./vtu_to_vti_converter [VTU_FILEPATH] [X_RESOLUTION] [Y_RESOLUTION] [Z_RESOLUTION].
std::int32_t main(std::int32_t argc, char** argv)
{
  auto filepath  = std::string(argv[1]);

  auto reader    = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
  auto converter = vtkSmartPointer<vtkCellDataToPointData>      ::New();
  auto resampler = vtkSmartPointer<vtkResampleToImage>          ::New();
  auto writer    = vtkSmartPointer<vtkXMLImageDataWriter>       ::New();

  std::cout << "Reading unstructured grid.\n";
  reader   ->SetFileName              (filepath.c_str());
  reader   ->UpdateInformation        ();
  for (auto i = 0; i < reader->GetNumberOfCellArrays(); ++i)
    reader ->SetCellArrayStatus       (reader->GetCellArrayName(i), std::string(reader->GetCellArrayName(i)) == std::string("Q") ? 1 : 0);
  reader   ->Update                   ();
  auto input_data     = reader->GetOutput();

  std::cout << "Converting cell data to point data.\n";
  converter->SetInputDataObject       (input_data);
  converter->SetPassCellData          (false);
  converter->SetContributingCellOption(vtkCellDataToPointData::ContributingCellEnum::All);
  converter->Update                   ();
  auto converted_data = converter->GetOutput();

  std::cout << "Resampling unstructured grid to structured grid.\n";
  resampler->SetInputDataObject       (converted_data);
  resampler->SetUseInputBounds        (true);
  resampler->SetSamplingDimensions    (std::stoi(argv[2]), std::stoi(argv[3]), std::stoi(argv[4]));
  resampler->Update                   ();
  auto resampled_data = resampler->GetOutput();

  std::cout << "Writing structured grid.\n";
  writer   ->SetInputData             (resampled_data);
  writer   ->SetFileName              ((filepath.substr(0, filepath.size() - 3) + std::string("vti")).c_str());
  writer   ->Update                   ();

  return 0;
}
## Oxyflame Rendering Tools

### Dependencies
- GCC/MSVC
- CMake
- Ospray
- VTK

### Building
- Run CMake in the `offline_ray_tracer` subdirectory.
- Build the output of CMake with GCC/MSVC.

### Running
- Modify the dataset location and rendering preferences in `settings.json`.
- Run as `offline_ray_tracer [PATH_TO_SETTINGS_JSON]`.

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <array>
#include <cstdint>
#include <fstream>
#include <string>
#include <vector>

#include <vtk_jsoncpp.h>

namespace rt
{
struct settings
{
  struct data_filepath
  {
    std::string geometry;
    std::string volume  ;
    std::string scalar  ;
  };
  struct key_frame
  {
    double                time    ;
    std::array<double, 3> position;
    std::array<double, 3> center  ;
    std::array<double, 3> up      ;
  };

  explicit settings  (const std::string& filepath)
  {
    std::ifstream   file_stream(filepath);
    vtkJson::Reader reader;
    vtkJson::Value  root;
    reader.parse(file_stream, root);

    for (auto& iteratee : root["data_file_paths"])
    {
      auto& entry = data_file_paths.emplace_back();
      if (iteratee.isMember("geometry")) entry.geometry = iteratee["geometry"].asString();
      if (iteratee.isMember("volume"  )) entry.volume   = iteratee["volume"  ].asString();
      if (iteratee.isMember("scalar"  )) entry.scalar   = iteratee["scalar"  ].asString();
    }

    if (root.isMember("transfer_function"))
      transfer_function = root["transfer_function"].asString();

    time_scale      =  root["time_scale"     ]   .asDouble();
    loop            =  root["loop"           ]   .asBool  ();

    image_size      = {root["image_size"     ][0].asInt   (),
                       root["image_size"     ][1].asInt   ()};
    step_size       =  root["step_size"      ]   .asDouble();
    samples         =  root["samples"        ]   .asInt   ();
    ambient_samples =  root["ambient_samples"]   .asInt   ();
    shadows         =  root["shadows"        ]   .asBool  ();

    update_rate     =  root["update_rate"    ]   .asDouble();

    for (auto& iteratee : root["key_frames"])
    {
      auto& entry    = key_frames.emplace_back();
      entry.time     = iteratee["time"].asDouble();
      entry.position = std::array<double, 3> {
                       iteratee["position"][0].asDouble(),
                       iteratee["position"][1].asDouble(),
                       iteratee["position"][2].asDouble()};
      entry.center   = std::array<double, 3> {
                       iteratee["center"  ][0].asDouble(),
                       iteratee["center"  ][1].asDouble(),
                       iteratee["center"  ][2].asDouble()};
      entry.up       = std::array<double, 3> {
                       iteratee["up"      ][0].asDouble(),
                       iteratee["up"      ][1].asDouble(),
                       iteratee["up"      ][2].asDouble()};
    }
  }
  settings           (const settings&  that) = default;
  settings           (      settings&& temp) = default;
 ~settings           ()                      = default;
  settings& operator=(const settings&  that) = default;
  settings& operator=(      settings&& temp) = default; 

  // Input settings.
  std::vector<data_filepath>  data_file_paths   = {};
  std::string                 transfer_function = std::string();
  double                      time_scale        = 1000.0;
  bool                        loop              = true;

  // Ray tracer settings.
  std::array<std::int32_t, 2> image_size        = {4096, 2160};
  double                      step_size         = 0.1;
  std::int32_t                samples           = 8;
  std::int32_t                ambient_samples   = 0;
  bool                        shadows           = false;

  // Animation settings.
  double                      update_rate       = 16.0;
  std::vector<key_frame>      key_frames        = {};
};
}

#endif

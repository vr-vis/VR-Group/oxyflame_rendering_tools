#ifndef TRANSFER_FUNCTION_HPP
#define TRANSFER_FUNCTION_HPP

#include <cstdint>
#include <fstream>
#include <string>

#include <vtk_jsoncpp.h>
#include <vtkColorTransferFunction.h>
#include <vtkPiecewiseFunction.h>
#include <vtkSmartPointer.h>

namespace rt
{
class transfer_function
{
public:
  explicit transfer_function  (const std::string& filepath)
  {
    std::ifstream   file_stream(filepath);
    vtkJson::Reader reader;
    vtkJson::Value  root;
    reader.parse(file_stream, root);

    auto opacity = root[0]["Points"   ];
    auto color   = root[0]["RGBPoints"];
    auto space   = root[0]["ColorSpace"].asString();

    for (std::uint32_t i = 0; i < opacity.size(); i += 4)
      opacity_function->AddPoint(
        opacity[i    ].asDouble(), 
        opacity[i + 1].asDouble());
    for (std::uint32_t i = 0; i < color.size(); i += 4)
      color_function->AddRGBPoint(
        color  [i    ].asDouble(), 
        color  [i + 1].asDouble(), 
        color  [i + 2].asDouble(), 
        color  [i + 3].asDouble());

    if      (space == "RGB")       color_function->SetColorSpace(VTK_CTF_RGB);
    else if (space == "HSV")       color_function->SetColorSpace(VTK_CTF_HSV);
    else if (space == "LAB")       color_function->SetColorSpace(VTK_CTF_LAB);
    else if (space == "CIEDE2000") color_function->SetColorSpace(VTK_CTF_LAB_CIEDE2000);
    else if (space == "Diverging") color_function->SetColorSpace(VTK_CTF_DIVERGING);
    else if (space == "Step")      color_function->SetColorSpace(VTK_CTF_STEP);
  }
  transfer_function           (const transfer_function&  that) = delete ;
  transfer_function           (      transfer_function&& temp) = delete ;
 ~transfer_function           ()                               = default;
  transfer_function& operator=(const transfer_function&  that) = delete ;
  transfer_function& operator=(      transfer_function&& temp) = delete ; 

  vtkNew<vtkPiecewiseFunction>     opacity_function;
  vtkNew<vtkColorTransferFunction> color_function  ;
};
}

#endif
